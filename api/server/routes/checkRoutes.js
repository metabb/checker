import { Router } from 'express';
import BookController from '../controllers/checkController';

const router = Router();

// router.get('/', BookController.getAllBooks);
// router.post('/', BookController.addBook);
router.post('/checkMember', BookController.checkMembers);
router.post('/checkHistory', BookController.checkHistory);
// router.post('/mem/:account_code/:account_name/:ip_address/:reff_code', BookController.checkMembers);
// router.post('/his/:transaction_id/:account_name/:account_code/:bank_name/:reff_code/:ammount/:confirmed_by/:confirmed_date', BookController.checkHistory);
// router.put('/:id', BookController.updatedBook);
// router.delete('/:id', BookController.deleteBook);

export default router;