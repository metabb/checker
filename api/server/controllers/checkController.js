import BookService from '../services/checkService';
import UserService from '../services/userService';
import memberService from '../services/memberService';
import historyService from '../services/historyService';
import Util from '../utils/Utils';


const util = new Util();

class checkController {

    static async getAMember(req, res) {
        const { id } = req.params;

        try {
            const theBook = await UserService.getAMember(id);

            if (!theBook) {
                util.setError(404, `Cannot find Member with the Reff Code ${id}`);
            } else {
                util.setSuccess(200, 'Found Member ', theBook);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }




    }



    static async checkMembers(req, res) {

        const data = req.body.data;
        let success = 0;
        let duplicated = 0;
        let error = 0;
        for (let i = 0; i < data.length; i++) {
            console.log("looping 1");
            let childArray = data[i];
            const account_code = childArray.account_code;
            const account_name = childArray.account_name;
            const ip_address = childArray.ip_address;
            const reff_code = childArray.reff_code;

            try {
                const theUser = await UserService.getAMember(reff_code);
                const theMember = await memberService.getAMembers(account_code);

                if (!theUser) {
                    util.setError(404, `Cannot find Member with the Reff Code ${reff_code}`);
                    error++;
                } else if (theUser && !theMember) {

                    try {
                        const addMember = await memberService.addMember(account_code, account_name, ip_address, reff_code);

                        if (addMember) {
                            util.setSuccess(200, 'Success Insert Account Member ');
                            success++;
                        }
                        // return util.send(res);
                    } catch (error) {
                        util.setError(404, error);
                        // return util.send(res);
                    }


                } else if (theUser && theMember) {
                    util.setError(404, `Duplicated Data ${account_code}`);
                    duplicated++;
                }
                // return util.send(res);
            } catch (error) {
                util.setError(404, error);
                // return util.send(res);
            }
            // return util.send(res);



        }



        util.setSuccess(200, `Success Insert Account ${success} Member | Error Insert Account  ${error} Member | Duplicated  ${duplicated} Member`);
        return util.send(res);


    }


    static async checkHistory(req, res) {

        const data = req.body.data;
        let success = 0;
        let duplicated = 0;
        let error = 0;
        for (let i = 0; i < data.length; i++) {
            console.log("looping 1");
            let childArray = data[i];
            const transaction_id = childArray.transaction_id;
            const account_name = childArray.account_name;
            const account_code = childArray.account_code;
            const bank_name = childArray.bank_name;
            const reff_code = childArray.reff_code;
            const ammount = childArray.ammount;
            const confirmed_by = childArray.confirmed_by;
            const confirmed_date = childArray.confirmed_date;

            try {
                const theMember = await memberService.getAMembers(account_code);
                const theHistory = await historyService.getAHistory(transaction_id);

                if (!theMember) {
                    util.setError(404, `Cannot find Member with the Reff Code ${reff_code}`);
                    error++;
                } else if (!theHistory && theMember) {

                    try {
                        const addHistory = await historyService.addHistory(transaction_id, account_name, account_code, bank_name, reff_code, ammount, confirmed_by, confirmed_date);

                        if (addHistory) {
                            util.setSuccess(200, 'Success Insert Transaction History ');
                            success++;
                        }
                        // return util.send(res);
                    } catch (error) {
                        util.setError(404, "error insert");
                        error++;
                        // return util.send(res);
                    }


                } else if (theHistory && theMember) {
                    util.setError(404, `Duplicated Data`);
                    duplicated++;
                }

                // return util.send(res);
            } catch (error) {
                util.setError(404, error);
                // return util.send(res);
            }
        }

        util.setSuccess(200, `Success Insert Account ${success} Member | Error Insert Account  ${error} Member | Duplicated  ${duplicated} Member`);
        return util.send(res);

    }


}






export default checkController;