module.exports = (sequelize, DataTypes) => {
    const account_member = sequelize.define('account_member', {
        account_code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        account_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ip_address: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reff_code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {});
    return account_member;
};