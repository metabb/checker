module.exports = (sequelize, DataTypes) => {
    const transaction_history = sequelize.define('transaction_history', {
        transaction_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        account_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        account_code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        bank_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reff_no: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ammount: {
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        confirmed_by: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        confirmed_date: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    }, {});
    return transaction_history;
};