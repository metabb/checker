module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reff_code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    });

    return users;
};