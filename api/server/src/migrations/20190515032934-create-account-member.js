module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('account_members', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            account_code: {
                allowNull: false,
                type: Sequelize.STRING
            },
            account_name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            ip_address: {
                allowNull: false,
                type: Sequelize.STRING
            },
            reff_code: {
                allowNull: false,
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('account_members');
    }
};