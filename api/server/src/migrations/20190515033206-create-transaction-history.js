module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('transaction_histories', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            transaction_id: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            account_name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            account_code: {
                allowNull: false,
                type: Sequelize.STRING
            },
            bank_name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            reff_no: {
                allowNull: false,
                type: Sequelize.STRING
            },
            ammount: {
                allowNull: false,
                type: Sequelize.DECIMAL
            },
            confirmed_by: {
                allowNull: false,
                type: Sequelize.STRING
            },
            confirmed_date: {
                allowNull: false,
                type: Sequelize.DATE
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('transaction_histories');
    }
};