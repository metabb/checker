import database from '../src/models';

class userService {


    static async getAMember(id) {
        try {
            const theMember = await database.users.findOne({
                where: { reff_code: String(id) }
            });

            return theMember;
        } catch (error) {
            throw error;
        }
    }

}

export default userService;