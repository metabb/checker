import database from '../src/models';

class memberService {


    static async getAMembers(id) {
        try {
            const theMembers = await database.account_member.findOne({
                where: { account_code: String(id) }
            });

            return theMembers;
        } catch (error) {
            throw error;
        }
    }

    static async addMember(account_code, account_name, ip_address, reff_code) {
        try {
            return await database.account_member.create({
                account_code: account_code,
                account_name: account_name,
                ip_address: ip_address,
                reff_code: reff_code
            });
        } catch (error) {
            throw error;
        }
    }

}

export default memberService;