import database from '../src/models';

class historyService {


    static async getAHistory(id) {
        try {
            const theMembers = await database.transaction_history.findOne({
                where: { transaction_id: String(id) }
            });

            return theMembers;
        } catch (error) {
            throw error;
        }
    }

    static async addHistory(transaction_id, account_name, account_code, bank_name, reff_code, ammount, confirmed_by, confirmed_date) {
        try {
            return await database.transaction_history.create({
                transaction_id: transaction_id,
                account_name: account_name,
                account_code: account_code,
                bank_name: bank_name,
                reff_no: reff_code,
                ammount: ammount,
                confirmed_by: confirmed_by,
                confirmed_date: confirmed_date,
            });
        } catch (error) {
            throw error;
        }
    }

}

export default historyService;